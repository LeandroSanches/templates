$(document).ready(function() {
var x=0; //questoes certas
var y=0; //questoes marcadas


$( ".gabarito,.avancar,.voltar,.pegapergunta,.tela_final" ).hide();
exibirQuiz(1);
function calcularAproveitamento(certa,marcadas){
	var resul = (100*certa)/marcadas;
	var result2 = parseFloat(resul.toFixed(2));
	$('.aproveitamento').text(result2);
}
function avancarQuestoes(n){
	$( ".avancar" ).hide();
	var numeroQuestoes = $('.quizes').length;
	//alert(numeroQuestoes);
	atual = parseInt(n);
	proximo = n+1;
	anterior = n+1;
if(atual>numeroQuestoes-1){
	$('.exibi').hide();
	$('.tela_final').fadeIn();
	$('.respostas').text(y);
	$('.acertos').text(x);
	calcularAproveitamento(x,y);
}else{
	$('.q'+atual).hide();
	$('.q'+proximo).fadeIn(1000);
	exibirQuiz(proximo);
}
}
function exibirQuiz(nquiz){
	//alert('você está exibindo o quiz '+nquiz);
	//pega numero de questoes
	var numeroQuestoes = $('.conteudo').length;
	//exibi questao escolhida
	var exibiQuestao = $('.lista'+nquiz).html();
	// monta a questao
	$('.exibi').append('<div class="pegapergunta">'+nquiz+'</div>');
	$('.exibi').append(exibiQuestao);
}
  var pi=0;
  $( ".exibi" ).on( "click", ".resposta", function() {
	  //pontuacao inicial
		//pga letra marcada
		var letra=$(this).data('resposta');	
		var justificativa=$(this).data('justificativa');		
		//pega gabarito da questão
		var gabarito=$(this).parent().find('.gabarito').text();

		//confere gabarito
		if(gabarito===letra){
			x++;
			y++;
			pi++;
			$(this).append('<div class="acertou">Você acertou.<br>'+justificativa+'</div>');
			$(this).removeClass('resposta');
			$( ".avancar" ).fadeIn(1000);
		}else{
			y++;
			  $(this).append('<div class="errou">Você errou.<br>'+justificativa+'</div>');
			$(this).removeClass('resposta');
		}
});
    $(".avancar").click(function(){
		//avancarQuestoes(1);
		var fase =$('.pegapergunta').last().text();
		var inteiro= parseInt(fase);
		avancarQuestoes(inteiro);
    });
});
