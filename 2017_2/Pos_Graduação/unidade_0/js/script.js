 /*PARALLAX*/
$(document).ready(function(){
	
	//$("body").fadeOut(0);
	//$("body").fadeIn(1000);
	
         $window = $(window);
         $('section[data-type="background"]').each(function(){
           var $scroll = $(this);                 
            $(window).scroll(function() {
              var yPos = -($window.scrollTop() / $scroll.data('speed')); 
               var coords = '50% '+ yPos + 'px';
              $scroll.css({ backgroundPosition: coords });    
            });
         });  

/*TOPO*/
      $('.Topo').click(function(){
          $('html, body').animate({scrollTop:0}, 'slow');
      return false;
         });


/*SCROOL*/
 $('li a').click(function (event) {
   event.preventDefault();   
    var destino = 0;
    if ($(this.hash).offset().top > $(document).height() - $(window).height()) {
        destino = $(document).height() - $(window).height();
				
    } else {
        destino = $(this.hash).offset().top;
    }
     
    $('html,body').animate({
        scrollTop: destino 
    }, 1000, 'swing');
	
	if (window.matchMedia("(min-width: 1366px)").matches) {
		$('#nav-icon').toggleClass('open');
		$('#nav-icon').addClass('visible-lg');
	} else {
		$('#menu').fadeOut(400);
		$('#nav-icon').toggleClass('open');

	}
 
});

/*ÍCONE MENU*/
	$("#nav-icon").click(function () {
		$(this).toggleClass('open');
		$("#menu").fadeToggle(400);
	});

/*QUIZ*/
	$('.quiz .alternativa').on('click', function (){
            var gabarito = $(this).parent().parent().parent().data('correct');
            var elementPai = $('this').parent();
            var option = $(this).data('option');
            if(option == gabarito){
              $(this).css('background-color','#abcaaa');
              $(this).next( ".feedback-alternativa").stop(true,true).slideToggle().css('background-color','#abcaaa');
            }else{
              $(this).css('background-color','#c9a9a9');
                $(this).next( ".feedback-alternativa").stop(true,true).slideToggle().css('background-color','#c9a9a9');
            }
          });

          $('.quiz .next').on('click', function (){
              var elementOut = $(this).parent().parent().parent().parent();
              elementOut.fadeOut(500);
              var elementIn = $(this).data('next');
              $('.'+elementIn).delay(500).fadeIn(500);
          });

          $('.quiz .back').on('click', function (){
              var elementOut = $(this).parent().parent().parent().parent();
              elementOut.fadeOut(500);
              var elementIn = $(this).data('back');
              $('.'+elementIn).delay(500).fadeIn(500);
          });
          $('.resp').on('click', function (){
              $(this).children('.justificativa').slideToggle();
          });

/*GLOSSÁRIO*/		  
$('.significado-glossario').hide();

      //Efeito
        $('.palavra-glossario').click(function(){
          var abrir = $(this).data('open');
          $('.palavra-glossario').removeClass('active');
          $(this).toggleClass('active');
          $('.significado-glossario').slideUp();
          $('.'+abrir).stop(true,true).slideToggle();
        });
	
});




