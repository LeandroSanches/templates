# UNI - Web Framework Template Building

Framework megazord inspirado pelos conceitos do Material Design do Google e outros como Materialize e Uikit.

## Dependências e Inspirações

- Google Material Design
- Material Design Icons (https://materialdesignicons.com/ | https://cdn.materialdesignicons.com/4.5.95/)
- Google Fonts (PT Sans)
- Materialize
- Uikit (https://getuikit.com/docs/introduction)
- JQuery (3.4.1)
- Scrollify (https://github.com/lukehaas/scrollify)
- Scroll-Trigger
- Plyr (podcast | https://github.com/sampotts/plyr)
- Anicollection (only css | https://anicollection.github.io/)
- MathJax (Formulas | https://www.mathjax.org/)

## Temas

- Graduação - GRD
- Pós Graduação (semi-presencial) - POS
- Pós Graduação 100% EAD - POS100

### Subtemas

- Recurso - REC (usado para Apresentação, Encerramento e Recursos de Aprendizagem)

## Obs

**Notas**:

- Base - As pastas Fontes e Áudio são só para referência e evitar erros de desenvolvimento.
- Arquivos e pastas não utilizados devem ser deletados.
- custom.css: Arquivo local de personalização da unidade caso necessário. Por padrão ele não é inserido nos modelos de produção mas pode (e deve) ser usado se necessário.
- .rec - É uma classe utilitária dos recursos de tela única (apresentação, encerramento e etc). Ela não tem estilos atrelados.
- .mathjs - É a classe que ativa a conversão de formulas matemáticas de LaTex/Tex/Ascii para SVG/HTML. Deve ser aplicada a tag body.

---

**Referência de Produção**: https://www.notion.so/unigranrio/Modelo-Web-2010-1-bdca7e9daa1a4aa6bfd8848a70b37f08