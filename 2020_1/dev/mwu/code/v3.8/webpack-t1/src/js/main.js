// Imports
import "../sass/main.sass";

import $ from 'jquery';

// Global Vars

// Screen Vars
var firstScreen,
    lastScreen,
    activeScreen,
    hasCover = $("#cover").length

// Viewport Vars
var vpTop,
    vpBottom,
    vpStart,
    vpEnd,
    vpCover,
    viewportChecking = true,
    scrollThreshold = 0;

// console.log("FS: " + firstScreen + " | LS: " + lastScreen + " | AS: " + activeScreen);

var uni = (function () {
  var template = $("body").data("template");

  switch (template) {
    case 'grd':
      // Active Template Class
      $("body").addClass(template);
      // $("body").addClass("scroll-nav");
      // console.log("Template: "+template);
    break;
    case 'pos':
      $("body").addClass(template);
      // $("body").addClass("switcher-nav");
      // console.log("Template: " + template);
    break;
    case 'pos100':
      $("body").addClass(template);
      // $("body").addClass("switcher-nav");
      // console.log("Template: " + template);
    break;
  }

  // Create a new instance of HumanInput library
  // var hi = new HumanInput(window);
  var hi = function (target) {
    return new HumanInput(target, {
      noKeyRepeat: false,
      // logLevel: "DEBUG"
    });
  }

  // Cover Animation - Open/Close
  var coverAnimate = function (active) {
    // var coverActive = $("body").hasClass("cover-active");
    // var firstScreen = $(".content-index li").first().index();

    if (active) {
      screenLock("lock");
      scrollLock("free", 500);
      $("#cover").toggleClass("uk-animation-slide-top uk-animation-reverse");
      $("#main-content").toggleClass("uk-animation-fade");

      setTimeout(function () {
        $("#cover").toggleClass("hidden uk-animation-slide-top uk-animation-reverse");
        $("body").toggleClass("cover-active")
        screenLock("free");
      }, 1000);
    } else {
      screenLock("lock");
      scrollLock("lock", 500);
      $("#cover").toggleClass("hidden uk-animation-slide-top");
      $("#main-content").toggleClass("uk-animation-fade");

      setTimeout(function () {
        $("#cover").toggleClass("uk-animation-slide-top");
        $("body").toggleClass("cover-active")
        screenLock("free");
      }, 1000);
    }

    return coverAnimate;
  }

  // Content Controller
  var contentController = function () {
    var contents = $("#main-content > .section")
      .map(function () { return this.id; }) // convert to set of IDs
      .get();

    // console.log("CC IDs " + contents);

    var contentLinks = $("#uni #menu-nav:first [class*='nav-link'] a")
      .map(function () { return $(this).attr("href").replace("#", ""); }) // convert to set of IDs
      .get(); // convert to instance of Array (optional)

    // console.log("CC Links " + contentLinks);

    var contentOff = $("#main-content > .section[data-cc='off']")
      .map(function () { return this.id; }) // convert to set of IDs
      .get();

    // console.log("CC OFF ARR: ["+contentOff+"]");

    function compareArrays(arr1, arr2) {
      return $(arr1).not(arr2).length == 0 && $(arr2).not(arr1).length == 0
    };

    if (!compareArrays(contents, contentLinks)) {
      $.grep(contentLinks, function (el) {
        if ($.inArray(el, contents) == -1) {
          contentOff.push(el);
          // console.log("CC OFF ARR: [" + contentOff + "]");
        }
      });
    }

    $.each(contentOff, function (index, value) {
      // console.log("CC Link OFF EL: " + contentLinks.indexOf(value) + " : " + value);
      // console.log("CC ID OFF EL: " + contents.indexOf(value) + " : " + value);

      if (contentLinks.indexOf(value) > -1) {
        contentLinks.splice(contentLinks.indexOf(value), 1);
      }
      if (contents.indexOf(value) > -1) {
        contents.splice(contents.indexOf(value), 1);
      }

      $("#"+value).remove();
      $("[class*='nav-link'] [href$='#"+value+"']").parent().remove();
      // console.log("CC IDs " + contents);
      // console.log("CC Links " + contentLinks);
      // console.log("CC OFF ARR: [" + contentOff + "] | CC OFF Length: " + contentOff.length);
    });

    $.each(contentLinks, function (index, value) {
      // console.log("CC Links " + index + " : " + value);
      $("[class*='nav-link'] [href$='#" + value + "']").attr("data-switcher", index);
    });

    // $.each(contents, function (index, value) {
    // 	// console.log("CC IDs: " + index + " : " + value);
    // 	$("#"+value).addClass("ok");
    // });

    // console.log("CC Links " + contentLinks);
    // console.log("CC IDs " + contents);

    firstScreen = $(".content-index li").first().index();
    lastScreen = $(".content-index li").last().index();
    activeScreen = $(".content-index li[class*='active'] a").data("switcher");
    console.log("FS: " + firstScreen + " | LS: " + lastScreen + " | AS: " + activeScreen);

    return contentController;
  }

  // FAN - Floating Action Navigation (responsive sidebar)
  var floatingActionNav = function (nav, trigger) {
    $(trigger).on("click", function (e) {
      e.stopPropagation();
      // console.log("-- Click Trigger--")

      // $(htmlEl).toggleClass("fan-active");
      // $("body").toggleClass("fan-active");

      scrollLock("lock-free");

      $(trigger).toggleClass("opened closed");
      $(nav).toggleClass("opened closed");

      setTimeout(function () {
        $(".nav-full-screen").toggleClass("fixed");
      }, 200);
    })

    $(nav).on("click", function () {
      // console.log("-- Click Nav--")
      if ($(nav).hasClass("opened")) {

        $(trigger).toggleClass("opened closed");
        $(nav).toggleClass("opened closed");

        scrollLock("free", 300);
      }
    })

    return floatingActionNav;
  }

  // Screen Switcher (Uikit) Navigation
  var switcherNav = function (what, destination) {

    var activeNav = function (screenIndex) {
      $(".nav-switcher [data-switcher]").removeClass("active");
      $(".nav-switcher [data-switcher=" + screenIndex + "]").addClass("active");

      return activeNav;
    }

    var loopCheck = function (screenIndex) {
      // console.log(":B");
      if ($(".content-menu").hasClass("no-loop") || $("body").hasClass("no-loop")) {
        // var firstScreen = $(".content-index li").first().index();
        // var lastScreen = $(".content-index li").last().index();
        // console.log("FS: " + firstScreen + " | LS: " + lastScreen);
        if (screenIndex === lastScreen) {
          $(".content-menu .btn").removeClass("disabled opacity-50");
          $(".content-menu .next").addClass("disabled opacity-50");

          $(".menu-arrow a").removeClass("disabled opacity-50");
          $(".menu-arrow .next").addClass("disabled opacity-50");
          // console.log("Ultima tela: " + n);
        } else if (screenIndex === firstScreen) {
          $(".content-menu .btn").removeClass("disabled opacity-50");
          $(".content-menu .previous").addClass("disabled opacity-50");

          $(".menu-arrow a").removeClass("disabled opacity-50");
          $(".menu-arrow .previous").addClass("disabled opacity-50");
          // console.log("Primeira Tela: " + n);
        } else {
          $(".content-menu .btn").removeClass("disabled opacity-50");

          $(".menu-arrow a").removeClass("disabled opacity-50");
          // console.log("Outra Tela: " + n);
        }
      }

      return loopCheck;
    }

    var moveTo = function () {
      // var n = $(destination).data("switcher");

      // console.log("Screen: " + activeScreen);
      // console.log("Prev Screen: " + prevScreen);
      // console.log(" ");

      if (destination !== "undefined" || destination !== "") {
        screenLock("lock");
        scrollLock("lock");
        UIkit.switcher($('.content-index')).show(destination);
        activeScreen = $(".content-index li[class*='active'] a").data("switcher");

        // console.log("Screen: " + activeScreen);
        // console.log("Prev Screen: " + prevScreen);

        if (activeScreen !== prevScreen) {
          activeNav(activeScreen);
          loopCheck(activeScreen);
          $(document).scrollTop(0);

          // console.log("=/=");

          prevScreen = activeScreen;

          screenLock("free");
          scrollLock("free", 1000);
          // console.log("Screen: " + activeScreen);
          // console.log("Prev Screen: " + prevScreen);
        }
      }
    }

    switch (what) {
      case 'start':
        activeScreen = $(".content-index li[class*='active'] a").data("switcher");
        var prevScreen = activeScreen;

        console.log("Screen: " + activeScreen);
        console.log("Prev Screen: " + prevScreen);
        console.log(" -- START -- ");

        activeNav(activeScreen);
        loopCheck(activeScreen);
      break;
      case 'moveTo':
        moveTo();
      break;
    }
  }

  // Scroll Navigation
  var scrollNav = function () {
    $.scrollify({
      section: '.section',
      sectionName: 'section',
      easing: "easeOutExpo",
      standardScrollElements: ".scroll-defaut",
      // interstitialSection: "",
      offset: 0,
      scrollSpeed: 500,
      scrollbars: true,
      setHeights: true,
      overflowScroll: true,
      updateHash: false,
      touchScroll: false,
      before: function (index, sections) {
        // console.log("Before Index: " + index);
      },
      after: function (index, sections) {
        // console.log("After Index: " + index);
        $.scrollify.update()
      },
      afterResize: function () {
        // console.log("After Render: Update");
        $.scrollify.update();
      },
      afterRender: function () {
        // console.log("After Render: Update");
        $.scrollify.update();
      }
    });

    // Atualiza o tamanho das seções de acordo com o conteúdo
    // $(window).resize(function () {
    // 	$.scrollify.update();
    // });
    $(".section").on("click", function () {
      $.scrollify.update();
    });

    return scrollNav;
  }

  var scrollLock = function (action, timer) {
    switch (action) {
      case 'lock':
        if (timer) {
          setTimeout(function () {
            $("html, body").addClass("scroll-lock");
          }, timer);
        } else {
          $("html, body").addClass("scroll-lock");
        }
      break;
      case 'free':
        if (timer) {
          setTimeout(function () {
            $("html, body").removeClass("scroll-lock");
          }, timer);
        } else {
          $("html, body").removeClass("scroll-lock");
        }
      break;
      case 'lock-free':
        if (timer) {
          setTimeout(function () {
            $("html, body").toggleClass("scroll-lock");
          }, timer);
        } else {
          $("html, body").toggleClass("scroll-lock");
        }
      break;
    }

    return scrollLock;
  }

  var screenLock = function (action, timer) {
    switch (action) {
      case 'lock':
        if (timer) {
          setTimeout(function () {
            $("body").addClass("screen-lock");
          }, timer);
        } else {
          $("body").addClass("screen-lock");
        }
        break;
      case 'free':
        if (timer) {
          setTimeout(function () {
            $("body").removeClass("screen-lock");
          }, timer);
        } else {
          $("body").removeClass("screen-lock");
        }
        break;
      case 'lock-free':
        if (timer) {
          setTimeout(function () {
            $("body").toggleClass("screen-lock");
          }, timer);
        } else {
          $("body").toggleClass("screen-lock");
        }
        break;
    }

    return screenLock;
  }

  // Hide Topbar if Any
  var hideTopbar = function () {
    var wstPrev = $(window).scrollTop()
    $(window).scroll(function () {
      var wstNow = $(window).scrollTop();

      // console.log('scrolling ', wstPrev, wstNow, dh);

      if (wstPrev > wstNow) {
        $('.top-bar').removeClass('hide');
      } else {
        $('.top-bar').addClass('hide');
      }
      wstPrev = wstNow;

      return hideTopbar;
    });
  }

  var vpCheck = {
    top: function () {
      vpTop = $(window).scrollTop() == 0 ? true : false
    },
    bottom: function () {
      vpBottom = $(window).scrollTop() + $(window).height() == $(document).height() ? true : false
    },
    start: function () {
      vpStart = activeScreen == firstScreen ? true : false
    },
    end: function () {
      vpEnd = activeScreen == lastScreen ? true : false
    },
    cover: function () {
      vpCover = $("body").hasClass("cover-active") ? true : false
    },
    all: function () {
      vpTop = $(window).scrollTop() == 0 ? true : false
      vpBottom = $(window).scrollTop() + $(window).height() == $(document).height() ? true : false
      vpStart = activeScreen == firstScreen ? true : false
      vpEnd = activeScreen == lastScreen ? true : false
      vpCover = $("body").hasClass("cover-active") ? true : false
    },
    stop: function (timeout) {
      viewportChecking = false;
      console.log("VPC STOP")
      if (timeout) {
        setTimeout(function () {
          viewportChecking = true;
          console.log("VPC GO!")
        }, timeout);
      }
    }
  }

  // ---

  return {
    hi: hi, // Mini-function to instance of HumanInput. A handler for many inputs
    coverAnimate: coverAnimate, // Animate the cover "open/close"
    contentController: contentController, // Mount the content and nav index after load
    floatingActionNav: floatingActionNav, // Navbar (sider) with all controls off navigarion
    switcherNav: switcherNav, // Control screens change, animations and nav with Uikit switcher
    scrollNav: scrollNav, // Control the navigation by scoll if active
    scrollLock: scrollLock, // Lock or Free the scroll
    screenLock: screenLock, // Lock or Free the screen
    hideTopbar: hideTopbar, // Simple function to hide topbar on scroll if active
    vpCheck: vpCheck, // Check the viewport status for the navigation
  }
}());

// === Before Start Document ===
// Start Scroll from Beginning
$(window).on('beforeunload', function () {
  $(window).scrollTop(0);
});

// Players Audio (Podcast) & Video
var players = Plyr.setup('.js-player');

// Scroll to Target
jQuery.fn.extend({
  scrollTo: function (speed, easing) {
    return this.each(function () {
      var targetOffset = $(this).offset().top;
      $('html,body').animate({
        scrollTop: targetOffset
      }, speed, easing);
    });
  }
});

// === After Start Document ===
$(document).ready(function () {

  // === START ===
  uni.contentController();
  uni.vpCheck.all()

  // === NAVEGAÇÂO ===

  // COVER - Animação da Capa
  if (hasCover) {
    uni.scrollLock("lock")
    $("body").addClass("cover-active")
    $(".scroll-arrow").on("click", function (e) {
      e.preventDefault();
      uni.vpCheck.all()
      uni.vpCheck.stop(1000)
      uni.coverAnimate(vpCover)
    });
  }

  // NAVEGAÇÃO SLIDES (SWITCHER)
  if ($("body").hasClass("switcher-nav")) {
    console.log("> switcher-nav");
    uni.switcherNav("start");

    $(".nav-switcher [data-switcher]").on("click", function (e) {
      e.preventDefault();
      var screenTarget = $(this).data("switcher");

      uni.switcherNav("moveTo", screenTarget);
    });

    UIkit.util.on('#main-content', 'shown', function () {
      uni.vpCheck.all()
      console.log("vpTop: " + vpTop)
      console.log("vpBottom: " + vpBottom)
      console.log("vpStart: " + vpStart)
      console.log("vpEnd: " + vpEnd)
      console.log("vpCover: " + vpCover)
    })

    // CHECK PREV/START = scroll/wheel up, key up/left, swipe down - click prev
    uni.hi(window).on(['wheel:up', 'keydown:up', 'keydown:left', 'swipe:down', 'swipe:right'], function (event, key, code) {
      key ? console.log("> Key: " + key + " - Event: " + event.type) : console.log("> Event: " + event.type)

      if (viewportChecking) {
        // Check Viewport Status: start, end, top, bottom and cover active or not
        uni.vpCheck.all()

        // Scroll Up
        if (event.type == 'wheel' && !vpCover && vpTop && vpStart && hasCover) {
          // Cover Off and top of the first screen
          scrollThreshold++;

          if (scrollThreshold == 3) {
            uni.vpCheck.stop(2000)
            uni.coverAnimate(vpCover);
            scrollThreshold = 0;
          }
        } else if (event.type == 'wheel' && vpTop && !vpStart) {
          // Cover Off and top of the screen but not first screen
          scrollThreshold++;

          if (scrollThreshold == 3) {
            uni.vpCheck.stop(1000)
            uni.switcherNav("moveTo", "previous");
            scrollThreshold = 0;
          }
        }

        // Key Up
        if (event.type == 'keydown' && uni.hi(window).isDown('up') == true && hasCover) {
          if (!vpCover && vpTop && vpStart) {
            // Cover Off and top of the first screen
            uni.vpCheck.stop(1000)
            uni.coverAnimate(vpCover);
          } else if (!vpCover && vpTop && !vpStart) {
            // Cover Off and top of the screen but not first screen
            uni.vpCheck.stop(1000)
            uni.switcherNav("moveTo", "previous");
          }
        } else if (event.type == 'keydown' && !vpCover && !vpStart) {
          // Cover Off and key left
          uni.vpCheck.stop(1000)
          uni.switcherNav("moveTo", "previous");
        }

        // Swipe Down
        if (event.type == 'touchend' && uni.hi(window).isDown('swipe:down') == true && hasCover) {
          if (!vpCover && vpTop && vpStart) {
            // Cover Off and top of the first screen
            uni.vpCheck.stop(1000)
            uni.coverAnimate(vpCover);
          } else if (!vpCover && vpTop && !vpStart) {
            // Cover Off and top of the screen but not first screen
            uni.vpCheck.stop(1000)
            uni.switcherNav("moveTo", "previous");
          }
        } else if (event.type == 'touchend' && !vpCover && !vpStart) {
          // Cover Off and key right out of the first screen
          uni.vpCheck.stop(1000)
          uni.switcherNav("moveTo", "previous");
        }

      }

      console.log("vpTop: " + vpTop)
      console.log("vpBottom: " + vpBottom)
      console.log("vpStart: " + vpStart)
      console.log("vpEnd: " + vpEnd)
      console.log("vpCover: " + vpCover)
    })

    // CHECK NEXT/END   = scroll/wheel down, key down/righ/space, swipe up - click start (cover), click next
    uni.hi(window).on(['wheel:down', 'keydown:down', 'keydown:right', 'swipe:up', 'swipe:left'], function (event, key, code) {
      key ? console.log("> Key: " + key + " - Event: " + event.name) : console.log("> Event: " + event.type)

      if (viewportChecking) {
        // Check Viewport Status: start, end, top, bottom and cover active or not
        uni.vpCheck.all()

        // Scroll Down
        if (event.type == 'wheel' && vpCover && hasCover) {
          // Cover On
          scrollThreshold++;

          if (scrollThreshold == 3) {
            uni.vpCheck.stop(1000)
            uni.coverAnimate(vpCover);
            scrollThreshold = 0;
          }
        } else if (event.type == 'wheel' && vpBottom && !vpEnd) {
          // Cover off at bottom of screen and not the last screen
          scrollThreshold++;

          if (scrollThreshold == 3) {
            uni.vpCheck.stop(1000)
            uni.switcherNav("moveTo", "next");
            scrollThreshold = 0;
          }
        }

        // Key Down
        if (event.type == 'keydown' && uni.hi(window).isDown('down') == true && hasCover) {
          if (vpCover) {
            // Cover On
            uni.vpCheck.stop(1000)
            uni.coverAnimate(vpCover);
          } else if (vpBottom && !vpEnd) {
            // Cover Off
            uni.vpCheck.stop(1000)
            uni.switcherNav("moveTo", "next");
          }
        } else if (event.type == 'keydown' && !vpCover && !vpEnd) {
          // Key Right
          uni.vpCheck.stop(1000)
          uni.switcherNav("moveTo", "next");
        }

        // Swipe Up
        if (event.type == 'touchend' && uni.hi(window).isDown('swipe:up') == true && hasCover) {
          if (vpCover) {
            // Cover On
            uni.vpCheck.stop(1000)
            uni.coverAnimate(vpCover);
          } else if (vpBottom && !vpEnd) {
            // Cover Off
            uni.vpCheck.stop(1000)
            uni.switcherNav("moveTo", "next");
          }
        } else if (event.type == 'touchend' && !vpCover && !vpEnd) {
          // Swipe Left
          uni.vpCheck.stop(1000)
          uni.switcherNav("moveTo", "next");
        }
      }

      console.log("vpTop: " + vpTop)
      console.log("vpBottom: " + vpBottom)
      console.log("vpStart: " + vpStart)
      console.log("vpEnd: " + vpEnd)
      console.log("vpCover: " + vpCover)

    })
  }

  // FAN - Floating Action Nav
  if ($("body").hasClass("fan")) {
    uni.floatingActionNav(".nav-fan", "[class*='fan-trigger']");
  }

  // NAV OFF-CANVAS - Controles da Navegação
  $(".nav-off-canvas").sidenav({
    // Desativa/Ativa o scrollify na navegaçã mobile
    onOpenEnd: function () {
      $(".nav-oc-trigger").addClass("open");
      if ($("body").hasClass("scroll-nav")) {
        $.scrollify.disable();
      }
      // console.log('side nav is open');
    },
    onCloseEnd: function () {
      $(".nav-oc-trigger").removeClass("open");
      if ($("body").hasClass("scroll-nav")) {
        $.scrollify.enable();
      }
      // console.log('side nav is close');
    }
  });
  // Nav-oc-trigger on/off
  $(".nav-off-canvas li").on("click", function () {
    $(".nav-off-canvas").sidenav("close");
  });
  $(".nav-oc-trigger .close").on("click", function () {
    $(".nav-off-canvas").sidenav("close");
  });
  // Fix bug of sidenav link "close"
  $(".nav-oc-trigger").on("click", function (e) {
    e.preventDefault();
  });

  // NAV FLUTUANTE (bolinha)
  $('.fixed-action-btn').floatingActionButton({
    hoverEnabled: false
  });

  // NAVEGAÇÃO SCROLL
  if ($("body").hasClass("scroll-nav")) {
    // console.log("> scroll-nav");
    uni.scrollNav();
  }

  // TOPBAR - Hide topbar on scroll
  if ($("body").hasClass("top-bar-hidden")) {
    // console.log("> top-bar-hidden");
    uni.hideTopbar();
  }

  // Materialize Calls
  // M.AutoInit(); // Materialize 'All Components' Start (não funciona bem)
  $(".modal").modal();
  $(".dropdown-button").dropdown();
  $(".dropdown-trigger").dropdown();
  $(".carousel.carousel-slider").carousel({
    fullWidth: true,
    indicators: true
  });
  $(".materialboxed").materialbox({
    onOpenStart: function () {
      $(".top-bar").addClass("opacity-0");
      $(".content-nav").addClass("opacity-0");
    },
    onCloseStart: function () {
      $(".top-bar").removeClass("opacity-0");
      $(".content-nav").removeClass("opacity-0");
    }
  });
  $('.collapsible').collapsible();

  // Lightbox Transparent Image Fix
  $("[uk-lightbox] a").on("click", function () {
    var imgSrc = $(this).children("img").attr("src"),
      transparentImg = $(this).children("img").hasClass("transparent-img");
    // console.log("SRC: " + imgSrc);

    $(document).on('itemshow', $("[uk-lightbox]"), function () {
      if (transparentImg) {
        // console.log("it works: .uk-lightbox img[src*='" + imgSrc + "']");
        setTimeout(function () {
          $(".uk-lightbox img[src*='" + imgSrc + "']").addClass("transparent-img transition");
        }, 50);
      }
    });
  });

  // Topics Nav Sync
  $(".topics-nav li").on("click", function () {
    var i = ($(this).index()+1);
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".topics-nav li").removeClass("uk-active");
      $(".topics-nav li:nth-child("+i+")").addClass("uk-active");
    }, 200);
  });

  // MathJax
  if ($("body").hasClass("mathjs")) {
    window.MathJax = {
      tex: {
        inlineMath: [
          ['$', '$'],
          ['\\(', '\\)']
        ]
      },
      svg: {
        fontCache: 'global'
      }
    };

    (function () {
      var polyfill = document.createElement('script');
      polyfill.src = 'https://polyfill.io/v3/polyfill.min.js?features=es6';
      polyfill.async = true;
      document.body.appendChild(polyfill);

      var mjx = document.createElement('script');
      mjx.src = 'https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-svg.js';
      mjx.async = true;
      document.body.appendChild(mjx);
    })();
  }

  // DESAFIO
  $('.desafio .collapsible.expandable').collapsible({
    accordion: false
  });
  $(".desafio .collapsible-header").on("click", function () {
    if (!$(".desafio .challenge-fields").hasClass("active")) {
      $(".desafio .challenge-fields").addClass("active");
    } else {
      $(".desafio .challenge-fields").addClass("finished");
      $(".desafio .challenge-fields .challenge-answer .challenge-text").attr("disabled", "disabled");
    }
  });
});