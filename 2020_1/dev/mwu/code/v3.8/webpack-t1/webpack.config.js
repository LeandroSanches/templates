const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyPlugin = require('copy-webpack-plugin');
module.exports = {
  mode: "development",
  devtool: "source-map",
  // devtool: "none",
  // Webpack needs to know where to start the bundling process, so we define the main JS and Sass files, both under the "./src" directory
  entry: ["./src/js/main.js"],
  // This is where we define the path where Webpack will place the bundled JS file
  output: {
    path: path.resolve(__dirname, "dist/base"),
    // Specify the base path for all the assets within your application. This is relative to the output path, so in our case it will be ./app/assets
    // publicPath: "/js",
    // The name of the output bundle. Path is also relative to the output path
    filename: "js/[name].bundle.js"
  },
  // performance: {
  //   hints: false,
  //   maxEntrypointSize: 512000,
  //   maxAssetSize: 512000
  // },
  module: {
    // Array of rules that tells Webpack how the modules (output) will be created
    rules: [
      {
        // Apply rule for .js
        test: /\.(js)$/,
        exclude: /(node_modules)/, // Set loaders to transform files.
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      },
      {
        test: /\.(s[c|a]ss)$/,
        exclude: /(node_modules)/,
        use: [
          MiniCssExtractPlugin.loader,
          {loader: "css-loader",
            options: {
              url: false,
              sourceMap: true,
              importLoaders: 2
            }
          },
          {
            loader: "postcss-loader",
            options: {
              sourceMap: true
            }
          },
          // {loader: "resolve-url-loader"},
          {
            loader: "sass-loader",
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(html)$/,
        use: ["html-loader"]
      },
      {
        test: /\.(png|svg|jpe?g|gif)$/,
        use:[
          // {
          //   loader: "url-loader",
          //   options: {
          //     limit: 8192 // in bytes
          //   }
          // },
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "img",
              // publicPath: "../img",
              esModule: false
            }
          }
        ]
      }
    ]
  },
  externals: {
    jquery: 'jQuery'
  },
  plugins: [
    // Configuration clean and re-build dist folder
    new CleanWebpackPlugin({
      cleanAfterEveryBuildPatterns: ["dist/base"]
    }),
    // Configuration options for MiniCssExtractPlugin. Here I"m only indicating what the CSS outputted file name should be and the location
    new MiniCssExtractPlugin({
      filename: "css/[name].bundle.css"
    }),
    // Configuration options for HtmlWebpackPlugin. Exports your .html as a template to /dist
    new HtmlWebpackPlugin({
      // inject: false,
      hash: true,
      template: "./src/index.html",
      filename: "index.html"
    }),
    // Configuration to copy assets from/to without process
     new CopyPlugin([
      { from: 'src/js/template-loader.js', to: 'js/template-loader.js' },
      { from: 'src/js/activities.js', to: 'js/activities.js' },
      { from: 'src/js/vendor', to: 'js/vendor' },
      { from: 'src/templates', to: 'templates' },
      { from: 'src/favicon.ico', to: './favicon.ico' },
      { from: 'src/img', to: 'img' },
      { from: 'src/audio', to: 'audio' },
      { from: 'src/doc', to: 'doc' },
      { from: 'src/contents', to: 'contents' }
      // { from: 'src/contents', to: '../grd/unidade/contents' }
    ]),
  ]
};