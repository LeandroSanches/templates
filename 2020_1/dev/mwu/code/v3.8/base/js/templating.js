var info,
  // content,
  templatePath = "./templates/";
  // contentPath = "./contents/"


var contentIndex = $("#uni [data-cc='on']")
  .map(function () {
    return $(this).attr("data-content");
  }).get();

function getContents(url) {
  (function () {
    $.ajax({
      url: url,
      type: "GET",
      async: false,
      success: function (response) {
        // content = $(response).filter("[data-content='content']");
        info = JSON.parse($(response).filter("#template-info").html());
        console.log("Info: " + info.id);
        // console.log("Cont: " + content.html());
      },
      error: function (xhr, ajaxOptions, thrownError) {
          // content = false;
          info = false;
          // console.log("TR: " + thrownError);
      }
    });
  })();
}

function loadTemplate(i) {
  console.log("Template File: "+templatePath+info.template+" | Target: #uni [data-templateTarget='"+info.templateTarget+"']");

  $("#uni [data-templateTarget='"+info.templateTarget+"']").loadTemplate(templatePath+info.template, info, {
    async: false,
    isFile: true,
    append: true
  })
  // console.log("Tplt UW: #" + info.templateUnwrap);
  if (info.templateUnwrap) {
    $("#"+info.id).unwrap();
  }
}

function loadContent(i) {
  console.log("Content File: "+contentIndex[i]+" | Target: #"+info.id+" ['"+info.contentTarget+"']");

  $("#"+info.id+" [data-contentTarget='"+info.contentTarget+"']")
  .load(contentIndex[i] + " [data-content='content']", function () {
    $(this).children(':first').unwrap();
  });

  // console.log("UR: #"+info.id+" [data-content='content']");
  // console.log("UR: #"+info.id+" .sc");
  // $("#"+info.id+" .sc").unwrap(".section-content");
}

for (var i = 0; i < contentIndex.length; i++) {
  // getIndex("./js/template-index.json")

  // console.log("FILE: " + contentIndex[i]);

  getContents(contentIndex[i])
  // console.log(contentIndex[i])
  // console.log("CONT: " + content);
  // console.log("INFO: " + info);

  var defered = new $.Deferred();

  defered
  .done(function () {
    loadTemplate(i)
  })
  .done(function () {
    loadContent(i)
  });

  if (info != false) {

    defered.resolve(i)

    console.log("----: END :---");
    console.log("");
  } else {
    // console.log("----: END :---");
    // console.log("");
  }
}