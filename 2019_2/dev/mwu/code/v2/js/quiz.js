$(document).ready(function () {

	$(".abas").click(function () {
		//avancarQuestoes(1);
		var destino = $(this).data('destino');
		$('.ab').fadeOut(500);
		$('.' + destino).delay(500).fadeIn(500);
	});

	var x = 0; //questoes certas
	var y = 0; //questoes marcadas
	$('.respostas_exibidas').hide();
	//PEGAR GABARITO

	//Carregamento das Perguntas
	var p1 = $('.q1 .pergunta').html();
	var p2 = $('.q2 .pergunta').html();
	var p3 = $('.q3 .pergunta').html();
	var p4 = $('.q4 .pergunta').html();
	var p5 = $('.q5 .pergunta').html();

	//Escrita das Perguntas
	$('.aba1 .gabaritando').html('<strong>GABARITO</strong> ' + p1);
	$('.aba2 .gabaritando').html('<strong>GABARITO</strong> ' + p2);
	$('.aba3 .gabaritando').html('<strong>GABARITO</strong> ' + p3);
	$('.aba4 .gabaritando').html('<strong>GABARITO</strong> ' + p4);
	$('.aba5 .gabaritando').html('<strong>GABARITO</strong> ' + p5);

	//Carregamento das Quetões
	var p1r1 = $('.q1 .resposta_a').html();
	var p1r2 = $('.q1 .resposta_b').html();
	var p1r3 = $('.q1 .resposta_c').html();
	var p1r4 = $('.q1 .resposta_d').html();
	var p1r5 = $('.q1 .resposta_e').html();

	var p2r1 = $('.q2 .resposta_a').html();
	var p2r2 = $('.q2 .resposta_b').html();
	var p2r3 = $('.q2 .resposta_c').html();
	var p2r4 = $('.q2 .resposta_d').html();
	var p2r5 = $('.q2 .resposta_e').html();

	var p3r1 = $('.q3 .resposta_a').html();
	var p3r2 = $('.q3 .resposta_b').html();
	var p3r3 = $('.q3 .resposta_c').html();
	var p3r4 = $('.q3 .resposta_d').html();
	var p3r5 = $('.q3 .resposta_e').html();

	var p4r1 = $('.q4 .resposta_a').html();
	var p4r2 = $('.q4 .resposta_b').html();
	var p4r3 = $('.q4 .resposta_c').html();
	var p4r4 = $('.q4 .resposta_d').html();
	var p4r5 = $('.q4 .resposta_e').html();

	var p5r1 = $('.q5 .resposta_a').html();
	var p5r2 = $('.q5 .resposta_b').html();
	var p5r3 = $('.q5 .resposta_c').html();
	var p5r4 = $('.q5 .resposta_d').html();
	var p5r5 = $('.q5 .resposta_e').html();

	// Outras Preguntas
	var g1 = $('.q1 .gabarito').text();
	var g2 = $('.q2 .gabarito').text();
	var g3 = $('.q3 .gabarito').text();
	var g4 = $('.q4 .gabarito').text();
	var g5 = $('.q5 .gabarito').text();

	$('.gav1 ').html(g1);
	$('.gav2 ').text(g2);
	$('.gav3 ').text(g3);
	$('.gav4 ').text(g4);
	$('.gav5 ').text(g5);

	$('.1resp' + g1).addClass('acertou');
	$('.2resp' + g2).addClass('acertou');
	$('.3resp' + g3).addClass('acertou');
	$('.4resp' + g4).addClass('acertou');
	$('.5resp' + g5).addClass('acertou');

	// colorir gabarito acertou

	// pegar Justificativas

	var q1la = $('.q1 .resposta_a').data('justificativa');
	var q1lb = $('.q1 .resposta_b').data('justificativa');
	var q1lc = $('.q1 .resposta_c').data('justificativa');
	var q1ld = $('.q1 .resposta_d').data('justificativa');
	var q1le = $('.q1 .resposta_e').data('justificativa');


	var q2la = $('.q2 .resposta_a').data('justificativa');
	var q2lb = $('.q2 .resposta_b').data('justificativa');
	var q2lc = $('.q2 .resposta_c').data('justificativa');
	var q2ld = $('.q2 .resposta_d').data('justificativa');
	var q2le = $('.q2 .resposta_e').data('justificativa');

	var q3la = $('.q3 .resposta_a').data('justificativa');
	var q3lb = $('.q3 .resposta_b').data('justificativa');
	var q3lc = $('.q3 .resposta_c').data('justificativa');
	var q3ld = $('.q3 .resposta_d').data('justificativa');
	var q3le = $('.q3 .resposta_e').data('justificativa');

	var q4la = $('.q4 .resposta_a').data('justificativa');
	var q4lb = $('.q4 .resposta_b').data('justificativa');
	var q4lc = $('.q4 .resposta_c').data('justificativa');
	var q4ld = $('.q4 .resposta_d').data('justificativa');
	var q4le = $('.q4 .resposta_e').data('justificativa');

	var q5la = $('.q5 .resposta_a').data('justificativa');
	var q5lb = $('.q5 .resposta_b').data('justificativa');
	var q5lc = $('.q5 .resposta_c').data('justificativa');
	var q5ld = $('.q5 .resposta_d').data('justificativa');
	var q5le = $('.q5 .resposta_e').data('justificativa');



	//mostrar respostas

	//Questão 1
	if (q1la === "") {
		//$('.respa1 ').html(p1r1);
	} else {
		$('.respa1 ').html(p1r1 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q1la + '<br>');
	}
	if (q1lb === "") {
		//$('.respb1 ').html(p1r2);
	} else {
		$('.respb1 ').html(p1r2 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q1lb + '<br>');
	}
	if (q1lc === "") {
		//$('.respc1 ').html(p1r3);
	} else {
		$('.respc1 ').html(p1r3 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q1lc + '<br>');
	}
	if (q1ld === "") {
		//$('.respd1 ').html(p1r4);
	} else {
		$('.respd1 ').html(p1r4 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q1ld + '<br>');
	}
	if (q1le === "") {
		//$('.respe1 ').html(p1r5);
	} else {
		$('.respe1 ').html(p1r5 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q1le + '<br>');
	}

	//Questão 2
	if (q2la === "") {
		//$('.respa2 ').html(p2r1);
	} else {
		$('.respa2 ').html(p2r1 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q2la + '<br>');
	}
	if (q2lb === "") {
		//$('.respb2 ').html(p2r2);
	} else {
		$('.respb2 ').html(p2r2 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q2lb + '<br>');
	}
	if (q2lc === "") {
		//$('.respc2 ').html(p2r3);
	} else {
		$('.respc2 ').html(p2r3 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q2lc + '<br>');
	}
	if (q2ld === "") {
		//$('.respd2 ').html(p2r4);
	} else {
		$('.respd2 ').html(p2r4 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q2ld + '<br>');
	}
	if (q2le === "") {
		//$('.respe2 ').html(p2r5);
	} else {
		$('.respe2 ').html(p2r5 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q2le + '<br>');
	}

	//Questão 3
	if (q3la === "") {
		//$('.respa3 ').html(p3r1);
	} else {
		$('.respa3 ').html(p3r1 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q3la + '<br>');
	}
	if (q3lb === "") {
		//$('.respb3 ').html(p3r2);
	} else {
		$('.respb3 ').html(p3r2 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q3lb + '<br>');
	}
	if (q3lc === "") {
		//$('.respc3 ').html(p3r3);
	} else {
		$('.respc3 ').html(p3r3 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q3lc + '<br>');
	}
	if (q3ld === "") {
		//$('.respd2 ').html(p3r4);
	} else {
		$('.respd3 ').html(p3r4 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q3ld + '<br>');
	}
	if (q3le === "") {
		//$('.respe3 ').html(p3r5);
	} else {
		$('.respe3 ').html(p3r5 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q3le + '<br>');
	}

	//Questão 4
	if (q4la === "") {
		//$('.respa4 ').html(p4r1);
	} else {
		$('.respa4 ').html(p4r1 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q4la + '<br>');
	}
	if (q4lb === "") {
		//$('.respb4 ').html(p4r2);
	} else {
		$('.respb4 ').html(p4r2 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q4lb + '<br>');
	}
	if (q4lc === "") {
		//$('.respc4 ').html(p4r3);
	} else {
		$('.respc4 ').html(p4r3 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q4lc + '<br>');
	}
	if (q4ld === "") {
		//$('.respd4 ').html(p4r4);
	} else {
		$('.respd4 ').html(p4r4 + '<br><br> <strong>JUSTIFICATIVA:</strong>' + q4ld + '<br>');
	}
	if (q4le === "") {
		//$('.respe4 ').html(p4r5);
	} else {
		$('.respe4 ').html(p4r5 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q4le + '<br>');
	}

	//Questão 5
	if (q5la === "") {
		//$('.respa5 ').html(p5r1);
	} else {
		$('.respa5 ').html(p5r1 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q5la + '<br>');
	}
	if (q5lb === "") {
		//$('.respb5 ').html(p5r2);
	} else {
		$('.respb5 ').html(p5r2 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q5lb + '<br>');
	}
	if (q5lc === "") {
		//$('.respc5 ').html(p5r3);
	} else {
		$('.respc5 ').html(p5r3 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q5lc + '<br>');
	}
	if (q5ld === "") {
		//$('.respd5 ').html(p5r4);
	} else {
		$('.respd5 ').html(p5r4 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q5ld + '<br>');
	}
	if (q5le === "") {
		//$('.respe5 ').html(p5r5);
	} else {
		$('.respe5 ').html(p5r5 + '<br><br> <strong>JUSTIFICATIVA:</strong> ' + q5le + '<br>');
	}

	//mostrr respostas
	$(".gabarito,.avancar,.voltar,.pegapergunta,.tela_final").hide();
	exibirQuiz(1);

	function calcularAproveitamento(certa, marcadas) {
		var resul = (100 * certa) / marcadas;
		var result2 = parseFloat(resul.toFixed(2));
		$('.aproveitamento').text(result2);
		$('.bar').css("width", result2 + '%');
	}

	function avancarQuestoes(n) {
		$(".avancar").hide();
		var numeroQuestoes = $('.quizes').length;
		//alert(numeroQuestoes);
		atual = parseInt(n);
		proximo = n + 1;
		anterior = n + 1;
		if (atual > numeroQuestoes - 1) {
			$('.exibi').hide();
			$('.tela_final').fadeIn();
			$('.respostas_exibidas').fadeIn();

			$('.respostas').text(y);
			$('.acertos').text(x);
			calcularAproveitamento(x, y);
		} else {
			$('.q' + atual).hide();
			$('.q' + proximo).fadeIn(1000);
			exibirQuiz(proximo);
		}
	}

	function exibirQuiz(nquiz) {
		//alert('você está exibindo o quiz '+nquiz);
		//pega numero de questoes
		var numeroQuestoes = $('.conteudo').length;
		//exibi questao escolhida
		var exibiQuestao = $('.lista' + nquiz).html();
		// monta a questao
		$('.exibi').append('<div class="pegapergunta">' + nquiz + '</div>');
		$('.exibi').append(exibiQuestao);
	}

	var pi = 0;
	$(".exibi").on("click", ".resposta", function () {
		//pontuacao inicial
		//pga letra marcada
		var letra = $(this).data('resposta');
		var justificativa = $(this).data('justificativa');
		//pega gabarito da questão
		var gabarito = $(this).parent().find('.gabarito').text();

		//confere gabarito
		if (gabarito === letra) {
			x++;
			y++;
			pi++;
			$(this).append('<div class="acertou">Resposta correta.<br></div>');
			$(this).removeClass('resposta');
			$(".avancar").fadeIn(1000);
		} else {
			y++;
			$(this).append('<div class="errou">Resposta incorreta.<br></div>');
			$(this).removeClass('resposta');
		}
	});
	$(".avancar").click(function () {
		//avancarQuestoes(1);
		var fase = $('.pegapergunta').last().text();
		var inteiro = parseInt(fase);
		avancarQuestoes(inteiro);
	});
});