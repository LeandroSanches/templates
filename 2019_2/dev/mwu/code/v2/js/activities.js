// console.log("activities.js loaded");
var tries,
	points;


var score = (function () {
	var altSelect,
		altCorrect;

	var check = function (t) {
		altSelect = $(t).data("a");
		altCorrect = $("#exercicios .questions-nav [class*=\"active\"]").data("correct");
		if (altSelect === altCorrect) {
			console.log("Acertou! -> s: "+ altSelect +" = c: "+altCorrect);

			return 1;
		} else {
			console.log("Errou! -> s: " + altSelect + " = c: " + altCorrect);

			return 0;
		}
	}

	return {
		check: check
	}
})();

$("#exercicios .questions .alt").on("click", function(){
	console.log("Click!");
	console.log("This: " + $(this).attr("data-a"));

	if (score.check(this)) {
		console.log("This 2: " + $(this).attr("class"));
		// $(this + " .alt-feedback").addClass("correct");
	} else {
		// $(this + " .alt-feedback").addClass("wrong");
	}
});

// Navegação
$(".questions-nav .next").on("click", function (event) {
	event.preventDefault();
	UIkit.switcher($('.uk-subnav')).show('next');
});