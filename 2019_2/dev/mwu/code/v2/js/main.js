// console.log("main.js loaded");
var uni = (function () {
	var template = $("body").data("template");
	switch (template) {
		case 'grd':
			// Active Template Class
			$("body").addClass(template);
			$("body").addClass("scroll-nav");
			console.log("Template: "+template);
		break;
		case 'pos':
			$("body").addClass(template);
			$("body").addClass("switcher-nav");
			console.log("Template: " + template);
		break;
		case 'pos2':
			$("body").addClass(template);
			$("body").addClass("switcher-nav");
			console.log("Template: " + template);
		break;
		default:
			alert('Defina o Template!');
	}

	var activeNav = function (n) {
		$(".nav-switcher [data-switcher]").removeClass("active");
		$(".nav-menu li [data-switcher="+n+"]").addClass("active");

		// console.log("Active Screen: "+(screen != ));

		return activeNav;
	}

	var hideTopbar = function () {
		var wstPrev = $(window).scrollTop()
		$(window).scroll(function () {
			var wstNow = $(window).scrollTop();

			// console.log('scrolling ', wstPrev, wstNow, dh);

			if (wstPrev > wstNow) {
				$('.top-bar').removeClass('hide');
			} else {
				$('.top-bar').addClass('hide');
			}
			wstPrev = wstNow;

			return hideTopbar;
		});
	}

	// Cover Start Animation
	var coverAnimate = function () {
		$("#cover").addClass("uk-animation-slide-top uk-animation-reverse");
		$("#main-content").addClass("uk-animation-fade");

		// UIkit.switcher($('.content-index')).show(screen);

		setTimeout(function () {
			$("#cover").addClass("hidden");
			uni.activeNav(0); // Solução provisória... ¬¬
		}, 1000);

		return coverAnimate;
	}

	// Scroll Navigation
	var scrollNav = function () {
		$.scrollify({
			section: '.section',
			sectionName: 'section',
			easing: "easeOutExpo",
			standardScrollElements: ".scroll-defaut",
			scrollSpeed: 500,
			scrollbars: true,
			setHeights: true,
			overflowScroll: true
		});

		return scrollNav;
	}

	var switcherNav = function () {
		$(document).on('show', $(".content-index"), function () {
			screen = $(".content-index li[class*='active'] a").data("switcher");
			uni.activeNav(screen);
			console.log("Screen: " + screen);
		});
		// Screen Switch
		$(".nav-switcher [data-switcher]").on("click", function (e) {
			e.preventDefault();
			var n = $(this).data("switcher");
			if (n !== "undefined" || n !== "") {
				UIkit.switcher($('.content-index')).show(n);
			}
		});
		// Nav - Content Switcher
		$(".content-nav .next").on("click", function (e) {
			e.preventDefault();
			UIkit.switcher($('.content-index')).show('next');
		});
		$(".content-nav .previous").on("click", function (e) {
			e.preventDefault();
			UIkit.switcher($('.content-index')).show('previous');
		});

		return switcherNav;
	}

	// Content Controller
	var contents = $("#main-content > .section")
		.map(function () { return this.id; }) // convert to set of IDs
		.get();

	console.log("CC IDs " + contents);

	var contentLinks = $("#uni .nav-menu:first [class*='nav-link'] a")
		.map(function () { return $(this).attr("href").replace("#", ""); }) // convert to set of IDs
		.get(); // convert to instance of Array (optional)

	console.log("CC Links " + contentLinks);

	var contentOff = $("#main-content > .section[data-cc='off']")
		.map(function () { return this.id; }) // convert to set of IDs
		.get();

	console.log("CC OFF ARR: ["+contentOff+"]");

	// $.each(contentLinks, function (index, value) {
	// 	ix = contents.indexOf(contentLinks[index]);
	// 	if (ix == -1) {
	// 		contents.splice(ix, 1);
	// 		console.log("CC LINK OFF: " + ix + " : " + value);
	// 	}
	// });

	function compareArrays(arr1, arr2) {
		return $(arr1).not(arr2).length == 0 && $(arr2).not(arr1).length == 0
	};

	if (!compareArrays(contents, contentLinks)) {
		$.grep(contentLinks, function (el) {
			if ($.inArray(el, contents) == -1) {
				contentOff.push(el);
				console.log("CC OFF ARR: [" + contentOff + "]");
			}
		});
	}

	$.each(contentOff, function (index, value) {
		console.log("CC Link OFF EL: " + contentLinks.indexOf(value) + " : " + value);
		console.log("CC ID OFF EL: " + contents.indexOf(value) + " : " + value);

		if (contentLinks.indexOf(value) > -1) {
			contentLinks.splice(contentLinks.indexOf(value), 1);
		}
		if (contents.indexOf(value) > -1) {
			contents.splice(contents.indexOf(value), 1);
		}

		$("#"+value).remove();
		$("[class*='nav-link'] [href$='#"+value+"']").parent().remove();
		console.log("CC IDs " + contents);
		console.log("CC Links " + contentLinks);
		console.log("CC OFF ARR: [" + contentOff + "] | CC OFF Length: " + contentOff.length);
	});

	$.each(contentLinks, function (index, value) {
		console.log("CC Links " + index + " : " + value);
		$("[class*='nav-link'] [href$='#" + value + "']").attr("data-switcher", index);
	});

	$.each(contents, function (index, value) {
		console.log("CC IDs: " + index + " : " + value);
		$("#"+value).addClass("ok");
	});

	// $.each(contents, function (index, value) {
	// 	console.log("CC IDs: " + index + " : " + value);
	// 	// if ($("#"+value).data("cc") === "off") {
	// 	// 	$("#"+value).remove();
	// 	// 	$(".nav-menu .nav-link-"+index).remove();
	// 	// 	$(".content-index .nav-link-" + index).remove();
	// 	// 	console.log("CC ID OFF: " + index + " : " + value);
	// 	// }
	// });
	// console.log("CC IDs: "+ids);

	console.log("CC Links " + contentLinks);
	console.log("CC IDs " + contents);

	// Players Audio & Video
	var players = Plyr.setup('.js-player');

	// Scroll to Target
	jQuery.fn.extend({
		scrollTo: function (speed, easing) {
			return this.each(function () {
				var targetOffset = $(this).offset().top;
				$('html,body').animate({
					scrollTop: targetOffset
				}, speed, easing);
			});
		}
	});

	// ---

	return {
		coverAnimate: coverAnimate,
		hideTopbar: hideTopbar,
		activeNav: activeNav,
		scrollNav: scrollNav,
		switcherNav: switcherNav
	}
}());

$(document).ready(function () {
	// === NAVEGAÇÂO ===

	// NAVEGAÇÃO SLIDES (SWITCHER)
	if ($("body").hasClass("switcher-nav")) {
		var screen = $(".content-index li[class*='active'] a").data("switcher");

		console.log("Screen: " + screen);
		uni.switcherNav();
	}

	// NAV OFF-CANVAS - Controles da Navegação
	$(".nav-off-canvas").sidenav({
		// Desativa/Ativa o scrollify na navegaçã mobile
		onOpenEnd: function () {
			$(".nav-oc-trigger").addClass("open");
			if ($("body").hasClass("scroll-nav")) {
				$.scrollify.disable();
			}
			console.log('side nav is open');
		},
		onCloseEnd: function () {
			$(".nav-oc-trigger").removeClass("open");
			if ($("body").hasClass("scroll-nav")) {
				$.scrollify.enable();
			}
			console.log('side nav is close');
		}
	});
	// nav-oc-trigger on/off
	$(".nav-off-canvas li").on("click", function () {
		$(".nav-off-canvas").sidenav("close");
	});
	$(".nav-oc-trigger .close").on("click", function () {
		$(".nav-off-canvas").sidenav("close");
	});
	// Fix bug of sidenav link "close"
	$(".nav-oc-trigger").on("click", function (e) {
		e.preventDefault();
	});

	// NAV FLUTUANTE
	$('.fixed-action-btn').floatingActionButton({
		hoverEnabled: false
	});

	// NAVEGAÇÃO SCROLL
	if ($("body").hasClass("scroll-nav")) {
		uni.scrollNav();
	}

	// COVER - Animação da Capa (jQuery-Scroll-Trigger)
	if ($("#cover").hasClass("cover-animate")) {
		$('#cover').scrolltrigger({
			swipeMode: true,
			swipeUp: function () {
				uni.coverAnimate();
				console.log("SWUP");
			},
			scrollDown: function () {
				uni.coverAnimate();
				console.log("SCDOWN");
			}
		});

		$(".scroll-arrow").on("click", function (e) {
			e.preventDefault();
			uni.coverAnimate();
		});
	}

	// TOPBAR - Hide topbar on scroll
	if ($("body").hasClass("top-bar-hidden")) {
		uni.hideTopbar();
	}

	// Materialize Calls
	// M.AutoInit(); // Materialize 'All Components' Start (não funciona bem)
	$(".modal").modal();
	$(".dropdown-button").dropdown();
	$(".dropdown-trigger").dropdown();
	$(".carousel.carousel-slider").carousel({
		fullWidth: true,
		indicators: true
	});
	$(".materialboxed").materialbox({
		onOpenStart: function () {
			$(".top-bar").addClass("opacity-0");
		},
		onCloseStart: function () {
			$(".top-bar").removeClass("opacity-0");
		}
	});
	$('.collapsible.expandable').collapsible({
		accordion: false
	});

	// DESAFIO
	$("#desafio .collapsible-header").on("click", function () {
		if (!$("#desafio .challenge-fields").hasClass("active")) {
			$("#desafio .challenge-fields").addClass("active");
		} else {
			$("#desafio .challenge-fields").addClass("finished");
			$("#desafio .challenge-fields .challenge-answer .challenge-text").attr("disabled", "disabled");
		}
	});
});