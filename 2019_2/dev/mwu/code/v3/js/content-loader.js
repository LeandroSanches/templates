// console.log("cl.js loaded");
// (function () {
$(document).ready(function () {
	// console.log("/o/");
	var j;
	var quiz = $.getJSON("test.json", function (json) {
		// console.log("|o|");
		j = json;
	});
	quiz.complete(function () {
		$("#exercicios .questions > .q").each(function (q) {
			if (q <= 4) {
				q = q+1;
				console.log(q + ": #exercicios .questions #question-" + q);
				$("#exercicios .questions #question-" + q + " .question-description p").html(j[q-1].description);

				$("#exercicios .questions > .q .alternatives > .alt").each(function (a) {
					if (a <= 4) {
						a = a+1;
						console.log(q, a + ": #exercicios .questions #question-" + q + " .alternatives > .alt-" + a + " .alt-title > span: " + j[q-1].alternatives[a-1].answer);
						$("#exercicios .questions #question-" + q + " .alternatives > .alt-" + a + " .alt-title > span").html(j[q-1].alternatives[a-1].answer);
						$("#exercicios .questions #question-" + q + " .alternatives > .alt-" + a + " .alt-feedback > p").html(j[q-1].alternatives[a-1].fb);
					}
				});
			}
		});
	});
});
// })();