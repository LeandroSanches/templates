# UNI - Web Framework Template Building

Framework megazord inspirado pelos conceitos do Material Design do Google e outros como Materialize e Uikit.

## Dependências e Inspirações

- Google Material Design
- Material Design Icons
- Google Fonts
- Materialize
- Uikit
- JQuery
- Scrollify
- Scroll-Trigger
- Plyr
- Anicollection
- MathJax

## Temas

- Graduação - GRD
- Pós Graduação (semi-presencial) - POS
- Pós Graduação 100% EAD - POS100

### Subtemas

- Recurso - REC (usado para Apresentação, Encerramento e Recursos de Aprendizagem)

## Obs

**Online**:

- Base - As pastas Fontes e Áudio são só para referência e evitar erros de desenvolvimento.
- Arquivos e pastas não utilizados devem ser deletados.
- custom.css: Arquivo local de personalização da unidade caso necessário. Por padrão ele não é inserido nos modelos de produção mas pode (e deve) ser usado se necessário.
- .rec - É uma classe utilitária dos recursos de tela única (apresentação, encerramento e etc). Ela não tem estilos atrelados.
- .mathjs - É a classe que ativa a conversão de formulas matemáticas de LaTex/Tex/Ascii para SVG/HTML. Deve ser aplicada a tag body.