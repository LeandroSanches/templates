$(document).ready(function(e) {
	
//Variavais
var pgTotal = 20;	//variavel com o total de paginas
var pgAtual = 1;	//variavel com pagina atual
var pgMinima = 1;	//variavel com paginas quantidade de paginas minimas

var pg;
var zero;

var corBot = '#fff';
var opacity = '0.5';

var cor = 'rgba(0,0,0,1)'; 	//cor de todo o referência
var opacit = '0.7';

//Scripit da navegação
$(".botAvan, .botVolt").css('border','solid 2px' +corBot); 
$(".botAvan, .botVolt, .mostrador, .fundoMais, .menu_titul").css('color', corBot);	
$(".botAvan, .botVolt, .mostrador").css('opacity', opacity);

//Referência
$(".topRef").css('opacity', opacit);
$(".topRef").css('background-color',cor); 									//Para trocar a cor da referência
$(".fecharRef ").css('border-left','solid 1px' +corBot); 								//Para trocar a cor da borda-left do fechar referencia
$(".fecharRef ").css('border-bottom','solid 1px' +corBot); 							//Para trocar a cor da borda-bottom do fechar referencia
$(".fecharRef, .topRef, .referen").css('color', corBot);							
//Para trocar a cor 

//Paginação do conteudo
if(pgTotal < 10)
	zero = 0;	

if(pgTotal >= 10)
	zero = '';

if(pgAtual < 10)
	$(".mostrador").html("0"+pgAtual+" / "+zero+pgTotal);//Faz com que o mostrador receba a variavel
	
if(pgAtual >= 10)
	$(".mostrador").html(pgAtual+" / "+zero+pgTotal);//Faz com que o mostrador receba a variavel


if(pgAtual == pgTotal)//Faz a verificação do botao
	$(".botAvan").css("opacity", 0.1);					
	
if(pgAtual == pgMinima)//Faz a verificação do botao
	$(".botVolt").css("opacity", 0.1);	
						
	
$(".botAvan").click(function(e) {
	if(pgAtual < pgTotal){								
		pgAtual++;   		
		pg = "pg"+pgAtual+".html";
		$(".iframe_conteudo").stop().fadeOut(0).attr( "src", pg ).fadeIn(2000);	
		if(pgAtual < 10)
		  $(".mostrador").html("0"+pgAtual+" / "+zero+pgTotal );//Faz com que o mostrador receba a variavel
		if(pgAtual >= 10)
		  $(".mostrador").html(+pgAtual+" / "+zero+pgTotal);//Faz com que o mostrador receba a variavel
		if(pgAtual != pgMinima)
			$(".botVolt").css("opacity", opacity);			
		if(pgAtual == pgTotal)//Faz a verificação do botao
			$(".botAvan").css("opacity", 0.1);	
			
	}													
});

$(".botVolt").click(function(e) {
	if(pgAtual > pgMinima){								
		pgAtual--;
		pg = "pg"+pgAtual+".html";		
		$(".iframe_conteudo").stop().fadeOut(0).attr( "src", pg ).fadeIn(2000);					
		if(pgAtual < 10)
		  $(".mostrador").html("0"+pgAtual+" / "+zero+pgTotal);//Faz com que o mostrador receba a variavel
		if(pgAtual >= 10)
		  $(".mostrador").html(+pgAtual+" / "+zero+pgTotal);//Faz com que o mostrador receba a variavel
		if(pgAtual == pgMinima)
			$(".botVolt").css("opacity", 0.1);			
		if(pgAtual != pgTotal)//Faz a verificação do botao
			$(".botAvan").css("opacity", opacity);			
	}														
});

//Verificador do botao mais/fechar
var abrir = 0;
$(".botmais").click(function(e) {   
  switch(abrir){
	  case 0://aqui some tudo
		  $(".botmais").css('transform','rotate(0deg)'); 
		  $(".menu").stop().slideUp(); //Faz sumirem  
		  abrir++;
	  break;
	  
	  case 1: //aqui volta tudo
		  $(".botmais").css('transform','rotate(45deg)'); 
		  $(".menu").stop().slideDown(); //Faz aparecer		
		  abrir--;
	  break;
		
  }	
});

$(".menu").hide();
$(".menu").slideDown(600);  

//Ação da referência

$(".ref").hide();

$(".re").click(function(e) {
	$(".ref").slideDown();//Mostra a referencia 
});
$(".fecharRef").click(function(e) {
	$(".ref").slideUp();//Fecha a referencia
});

	




		//******************	FINAL DO TEMPLATE	******************//





	
// FADE-IN
		$(".fadein").click(function() {
			var tempo=$(this).attr('data-tempo');	//Busca o valor do data tempo da classe .fadein e armazena dentro da var tempo
			var del = $(this).attr('data-delay');	// Busca o valor do data delay da classe .fadein e armazena dentro da var del
			var destino=$(this).attr('data-destino'); //Busca em qual elemento será aplicado o efeito e armazena na variavel,este é o target do efeito.
		$('.'+destino).delay(del).fadeIn(tempo);		
	});
		
// FADE-OUT
		$(".fadeout").click(function(){
			var tempo=$(this).attr('data-tempo');	
			var del = $(this).attr('data-delay');
			var destino=$(this).attr('data-destino');
			
		$('.'+destino).delay(del).fadeOut(tempo);
				
	}); 
	
// Slide Down
		$(".slidedown").click(function(){
			var tempo=$(this).attr('data-tempo');	
			var del = $(this).attr('data-delay');
			var destino=$(this).attr('data-destino');
			
		$('.'+destino).delay(del).slideDown(tempo);
				
	}); 
	
// Slide UP
	
	$(".slideup").click(function(){
			var tempo=$(this).attr('data-tempo');	
			var del = $(this).attr('data-delay');
			var destino=$(this).attr('data-destino');
			
		$('.'+destino).delay(del).slideUp(tempo);		
	}); 
//Toggle
	
	$(".toggle").click(function(){
			var tempo=$(this).attr('data-tempo');	
			var del = $(this).attr('data-delay');
			var destino=$(this).attr('data-destino');
			
		$('.'+destino).delay(del).toggle(tempo);		
	}); 
	
//Criação de Div passando o tamanho
	
		var largura = $('.box').attr("data-largura"); // Pega o atributo data largura da classe que for criada(Neste caso é da classe box),o data largura será usado no width.Armazena na variavel largura
		var altura = $('.box').attr("data-altura");	// Pega o atributo data altura da classe que for criada(Neste caso é da classe box),o data altura será usado no height.Armazena na variavel altura
		$('.box').add(this).css("width",largura).css("height",altura); // Vai adicionar ao css do classe selecionado(A usada é box) o tamanho e altura conforme foi informado nas variaveis acima.
		
// TUDO 
		
		var largura=$('.t').attr('data-largura');
		var altura=$('.t').attr('data-altura');
		var padding =$('.t').attr('data-padding');
		
		$('.t').add(this).css("width",largura).css("height",altura).css("padding",padding);
		
// Video
		var total = $('.pai_video').attr("data-quantidade");
		var i;
		for(i=1;i<=total;i++){
			var result= $('.pai_video').attr('data-video'+i+'');	
			
			var nome = $('.'+result).attr('data-nome');
			var vlargura = $('.'+result).attr('data-vlargura');
			var valtura = $('.'+result).attr('data-valtura');
			var controle = $('.'+result).attr('data-controle');
			var autoplay = $('.'+result).attr('data-auto');
			
			if((vlargura=="") || (vlargura<=0)){
				vlargura=320;	
			}
			if((valtura=="") || (valtura<=0)){
				valtura=240;	
			}
			
			$('<video width="'+vlargura+'" height="'+valtura+'" '+controle+' '+autoplay+' "><source src="video/'+nome+'.mp4" type="video/mp4; codecs="avc1.42E01E, mp4a.40.2"><source src="video/'+nome+'.webm" type="video/webm; codecs="vp8, vorbis" /><source src="video/'+nome+'.ogv" type="video/ogg; codecs="theora, vorbis" /></video>').appendTo(".pai_video");
		}
		
//MODAL
		
		$( ".abrir-modal" ).click(function() {
		   var destino= $(this).attr('data-modal-destino');
		   var autofechar= $(this).attr('data-modal-autofechar');
		   var classe= $(this).attr('data-modal-classe');
		   var abertura= $(this).attr('data-modal-tempo');
		   var delay= $(this).attr('data-modal-delay');
		//auto fechar
		if(autofechar>0){
			 x= autofechar;
 			 $('.'+destino+" .fechar").text('Fecha em ' + x +' segundos' );

			 var myTimer=setInterval(function() {
					x--;
					$('.'+destino+" .fechar").text('Fecha em ' + x +' segundos' );
				if(x==0){
					$('.'+destino).fadeOut(1000);
					clearInterval(myTimer);
				}
 			}, 1000);
		} 
			$('.'+destino).delay(delay).fadeIn(abertura);
			$('.'+destino).addClass(classe);
		}); 
		$( ".fechar" ).click(function() {
		   $( this ).parent().slideUp(1000);
		});
		 
		$('.box').hide();
		$('.ac').click(function(){
				var box = $(this).attr('data-box');
		for(i=1;i<=6;i++){
				var b = 'b'+i;
				if(b == box)
					$('.'+box).slideDown(1000);
				else
					$('.'+b).slideUp();
		}
			}); 			
});

// Padrao do tarja titulo
$(document).ready(function(e) {
	$(".animacao_tarja_titulo").fadeIn().delay(1000).animate({left:5, top:10},1000);
});

// Padrao do tarja titulo 2
$(document).ready(function(e) {
	$(".animacao_tarja_titulo2").fadeIn().delay(1000).animate({left:5, top:10},1000);
});	
	
// Piscar o botão
setInterval(function(){
   $(".piscar").animate({opacity:'toggle', marginTop:15},'slow')
})
 
//indicador de clique 
$(document).ready(function(e) {
	$(".animacao_clique").hide()
	  $(".animacao_clique").delay(1000).fadeIn().animate({
			  top:320 ,
			  left:724
	  },1000);
	  $(".animacao_clique").animate({/* Diferenda de 12 px */		
		  width: 50,				
		  height:62
		},1000);
	  $(".animacao_clique").animate({/* Mesmo tamanho do padrão */		
		  width: 70,				
		  height:84
	  },1000);
	  $(".animacao_clique").animate({
			  top:620 ,
			  left:924
	  },1000);
	  $(".animacao_clique").hide(0)
});

//indicador de escrita 
$(document).ready(function(e) {
	$(".animacao_text").hide()
    $(".animacao_text").delay(1000).fadeIn().animate({
			left: 0,
			top: 0
    },1000);
	$(".animacao_text").animate({
			left: 10,
			top: 5,
			width:	20
    },500);
	$(".animacao_text").animate({
			left: 0,
			top: 0,
			width:	40
    },500);
	$(".animacao_text").animate({
			left: 200,
    },2000);
	$(".animacao_text").hide(0)
});