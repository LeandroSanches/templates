$(document).ready(function(e) {

//Variavais
var pgTotal = 2;	//variavel com o total de paginas
var pgAtual = 1;	//variavel com pagina atual
var pgMinima = 1;	//variavel com paginas quantidade de paginas minimas

var pg = 1;
var zero;

var corBot = '#fff';
var opacity = '0.5';

//Scrip da navegação
$(".botAvan, .botVolt").css('border','solid 2px' +corBot); 
$(".botAvan, .botVolt, .mostrador").css('color', corBot);	
$(".botAvan, .botVolt, .mostrador").css('opacity', opacity);


//Paginação do conteudo
if(pgTotal < 10)
	zero = 0;	

if(pgTotal >= 10)
	zero = '';

if(pgAtual < 10)
	$(".mostrador").html("0"+pgAtual+" / "+zero+pgTotal);//Faz com que o mostrador receba a variavel
	
if(pgAtual >= 10)
	$(".mostrador").html(pgAtual+" / "+zero+pgTotal);//Faz com que o mostrador receba a variavel


if(pgAtual == pgTotal)//Faz a verificação do botao
	$(".botAvan").css("opacity", 0.1);					
	
if(pgAtual == pgMinima)//Faz a verificação do botao
	$(".botVolt").css("opacity", 0.1);	
						
	
$(".botAvan").click(function(e) {
	if(pgAtual < pgTotal){								
		pgAtual++;   		
		pg = "pg"+pgAtual+".html";		
		$(".iframe_conteudo").hide(0);
		$(".iframe_conteudo").attr( "src", pg );
		$(".iframe_conteudo").fadeIn(0);	
		if(pgAtual < 10)
		  $(".mostrador").html("0"+pgAtual+" / "+zero+pgTotal );//Faz com que o mostrador receba a variavel
		if(pgAtual >= 10)
		  $(".mostrador").html(+pgAtual+" / "+zero+pgTotal);//Faz com que o mostrador receba a variavel
		if(pgAtual != pgMinima)
			$(".botVolt").css("opacity", opacity);
	}
	if(pgAtual == pgTotal){
		pgRef = "referencias.html";
		$(".iframe_conteudo").hide(0);
		$(".iframe_conteudo").attr( "src", pgRef );
		$(".iframe_conteudo").fadeIn(0);
		$(".botAvan").css("opacity", 0.1);
	};													
});

$(".botVolt").click(function(e) {
	if(pgAtual > pgMinima){								
		pgAtual--;
		pg = "pg"+pgAtual+".html";		
		$(".iframe_conteudo").hide(0);
		$(".iframe_conteudo").attr( "src", pg );
		$(".iframe_conteudo").fadeIn(0);						
		if(pgAtual < 10)
		  $(".mostrador").html("0"+pgAtual+" / "+zero+pgTotal);//Faz com que o mostrador receba a variavel
		if(pgAtual >= 10)
		  $(".mostrador").html(+pgAtual+" / "+zero+pgTotal);//Faz com que o mostrador receba a variavel
		if(pgAtual == pgMinima)
			$(".botVolt").css("opacity", 0.1);			
		if(pgAtual != pgTotal)//Faz a verificação do botao
			$(".botAvan").css("opacity", opacity);			
	}
	if(pgAtual == pgTotal){
		pg = "pg"+pgAtual+".html";
		$(".iframe_conteudo").hide(0);
		$(".iframe_conteudo").attr( "src", pg );
		$(".iframe_conteudo").fadeIn(0);
	};
														
});

// Animações desafios //
$(".titulo").css('animation-duration','2s');
$("#box_desafio, .botao-forum, .textoConteudo").css('animation-duration','3s');
$(".imagem_index").css('animation-duration','4s');
$(".subtextoConteudo").css('animation-duration','4s');


		//******************	FINAL DO TEMPLATE	******************//
	
// FADE-IN
		$(".fadein").click(function() {
			var tempo=$(this).attr('data-tempo');	//Busca o valor do data tempo da classe .fadein e armazena dentro da var tempo
			var del = $(this).attr('data-delay');	// Busca o valor do data delay da classe .fadein e armazena dentro da var del
			var destino=$(this).attr('data-destino'); //Busca em qual elemento será aplicado o efeito e armazena na variavel,este é o target do efeito.
		$('.'+destino).delay(del).fadeIn(tempo);		
	});
		
// FADE-OUT
		$(".fadeout").click(function(){
			var tempo=$(this).attr('data-tempo');	
			var del = $(this).attr('data-delay');
			var destino=$(this).attr('data-destino');
			
		$('.'+destino).delay(del).fadeOut(tempo);
				
	}); 
	
// Slide Down
		$(".slidedown").click(function(){
			var tempo=$(this).attr('data-tempo');	
			var del = $(this).attr('data-delay');
			var destino=$(this).attr('data-destino');
			
		$('.'+destino).delay(del).slideDown(tempo);
				
	}); 
	
// Slide UP
	
	$(".slideup").click(function(){
			var tempo=$(this).attr('data-tempo');	
			var del = $(this).attr('data-delay');
			var destino=$(this).attr('data-destino');
			
		$('.'+destino).delay(del).slideUp(tempo);		
	}); 
	
//Toggle
	
	$(".toggle").click(function(){
			var tempo=$(this).attr('data-tempo');	
			var del = $(this).attr('data-delay');
			var destino=$(this).attr('data-destino');
			
		$('.'+destino).delay(del).toggle(tempo);		
	}); 
	
//Criação de Div passando o tamanho
	
		var largura = $('.box').attr("data-largura"); 
		// Pega o atributo data largura da classe que for criada(Neste caso é da classe box),o data largura será usado no width.Armazena na variavel largura
		var altura = $('.box').attr("data-altura");	
		// Pega o atributo data altura da classe que for criada(Neste caso é da classe box),o data altura será usado no height.Armazena na variavel altura
		$('.box').add(this).css("width",largura).css("height",altura); 
		// Vai adicionar ao css do classe selecionado(A usada é box) o tamanho e altura conforme foi informado nas variaveis acima.
		
// TUDO 
		
		var largura=$('.t').attr('data-largura');
		var altura=$('.t').attr('data-altura');
		var padding =$('.t').attr('data-padding');
		
		$('.t').add(this).css("width",largura).css("height",altura).css("padding",padding);		
		
//MODAL
		
		$( ".abrir-modal" ).click(function() {
		   var destino= $(this).attr('data-modal-destino');
		   var autofechar= $(this).attr('data-modal-autofechar');
		   var classe= $(this).attr('data-modal-classe');
		   var abertura= $(this).attr('data-modal-tempo');
		   var delay= $(this).attr('data-modal-delay');
		//auto fechar
		if(autofechar>0){
			 x= autofechar;
 			 $('.'+destino+" .fechar").text('Fecha em ' + x +' segundos' );

			 var myTimer=setInterval(function() {
					x--;
					$('.'+destino+" .fechar").text('Fecha em ' + x +' segundos' );
				if(x==0){
					$('.'+destino).fadeOut(1000);
					clearInterval(myTimer);
				}
 			}, 1000);
		} 
			$('.'+destino).delay(delay).fadeIn(abertura);
			$('.'+destino).addClass(classe);
		}); 
		$( ".fechar" ).click(function() {
		   $( this ).parent().slideUp(1000);
		});
		 
		$('.box').hide();
		$('.ac').click(function(){
				var box = $(this).attr('data-box');
		for(i=1;i<=6;i++){
				var b = 'b'+i;
				if(b == box)
					$('.'+box).slideDown(1000);
				else
					$('.'+b).slideUp();
		}
			}); 			


/////////////////// Pg 1: Conteúdo - Diminuir o Zoom //////////////////////////////

//CSS Inicial  var
var pagina= $('.bg_animado').attr('data-pagina');

//Captura de imagem
var imagem = $('.bg_animado').attr('data-bg');

var bg = "imagens/"+imagem;

$('.bg_animado').css('background-image', 'url(' + bg + ')');	

//////////////////////////////////////////////////////////////////////////////////

switch (pagina) {
	 case (pagina="zoomMenos"):
		  $(".bg_animado").css("width", 2500);
		  $(".bg_animado").animate({ width: 940}, 6000);
		  break;
	 case (pagina="zoomMais"):
		  $(".bg_animado").css("width", 940);
		  $(".bg_animado").delay(-1100).animate({ width: 1800}, 15000);	
		  break;
	 case (pagina="diagonal_esq_para_dir"):
	 	  $(".bg_animado").css("width", 940, "margin-top:", -200, "margin-left", -300);
	      $(".bg_animado").animate({ width: 1800}, 6000);
		  break;
	  default:
		  console.log('Insira um Background');
}       


/////////////////////// Data para capturar a largura //////////////////////////////

//Captura de largura para cada classe
var larg_titulo = $('.tituloConteudo').attr('data-lg-tit');

var larg_subTitulo = $('.subTitulo').attr('data-lg-sub');

$('.tituloConteudo').css('width', + larg_titulo); 

$('.subTitulo').css('width', + larg_subTitulo); 

switch (larg_titulo) {
	 case (larg_titulo=""):
		  $(".tituloConteudo").css("width", 600);
		  break;
}

switch (larg_subTitulo) {
case (larg_subTitulo=""):
		  $(".subTitulo").css("width", 550);
		  break;
}


/////////////////////////////////////////////////////////////////////////////////   
 
/*Pagina dos videos*/
$(".popupVideo, .botVideo").hide();

  $(".popupVideo").delay(1000).fadeIn();
  
  $(".fecharModal").on('click',function () {	 
	  $(".popupVideo").hide(600);
	  $(".botVideo").fadeIn(600);
	});
  
  $(".botVideo").click(function(e) {
	  $(".popupVideo").fadeIn();
});


/*Centralizado Vertical*/

var centralizadoVerticalaltura = $('.centralizadoVertical').height();
	centralizadoVerticalaltura = centralizadoVerticalaltura/2;
	centralizadoVerticalaltura = centralizadoVerticalaltura/-1;
	$('.centralizadoVertical').css('margin-top',centralizadoVerticalaltura);


});