$(document).ready(function () {

	$(".dropdown-trigger").dropdown();
	$(".button-collapse").sidenav();
	$('.sidenav').sidenav();
	$('.referencias').hide(0);



	$(".fechaReferencias").click(function () {

		$(this).removeClass('motrarreferencia2');
		$(this).addClass('motrarreferencia');
		$('.referencias').animate({
			bottom: -600
		});

				
		$('.motrarreferencia2').addClass('motrarreferencia');
		$('.motrarreferencia2').removeClass('motrarreferencia2');
		$('.referencias').hide(0);
	});


	$("footer").on("click", ".motrarreferencia", function () {

		$('.referencias').fadeIn(0);
		$('.referencias').animate({
			bottom: '0'
		});

		$(this).removeClass('motrarreferencia');
		$(this).addClass('motrarreferencia2');
	});
	
	$("footer").on("click", ".motrarreferencia2", function () {
		$(this).removeClass('motrarreferencia2');
		$(this).addClass('motrarreferencia');
		$('.referencias').animate({
			bottom: -600
		});
		$('.referencias').hide(0);
	});

	//diminuir rodape


	$("footer").on("click", ".esconder2", function () {
		$("footer h4,footer h5,.btns,.msgtexto").hide();
		$("footer").addClass('footer2');
		$(this).removeClass('esconder2');
		$('.ds i').addClass('cla');
		$(this).addClass('esconder3');
		//$( ".msgtexto" ).text('Mostrar');
		$(".msgtexto").hide();
		$('.referencias').animate({
			bottom: -600
		});

	});



	$("footer").on("click", ".esconder3", function () {
		$("footer h4,footer h5,.btns").show();
		$(this).removeClass('esconder3');
		$(this).addClass('esconder2');
		$('.ds i').removeClass('cla');
		$("footer").removeClass('footer2');
		$(".msgtexto").show();
		$(".msgtexto").text('Esconder');

	});







	$('.sidenav').sidenav();
	//dados do site
	$('.titulo h2').text(nomeUnidade);
	$('title').text(nomeUnidade);
	//contagem da barra
	$(".navegacao").click(function () {
		var valorDestino = $(this).data('destino');
		$(this).addClass('ativado ');
		$('.conteudoSetor').hide(200);
		$('.' + valorDestino).delay(200).slideDown(500);
		barraProgresso();
		return false;
	});
	$('.aba').hide();
	$('.ab1').show();
	// abas internas

	$(".abas li").click(function () {
		var valorDestino = $(this).data('destino');
		$('.aba').hide();
		$('.' + valorDestino).fadeIn();
	});



	function barraProgresso() {

		var navegacao = ($(".navegacao").length) / 2;
		var ativado = $(".ativado").length;
		var total = Math.ceil((100 * ativado) / navegacao);
		$('.evolucao').animate({
			width: total + '%'
		});
		$('.pnumber').text(total + '%');

	}



	//exibindo conteúdo
	if (transcricao == 'off') {
		$('.transcription').remove();
		$('.esc').removeClass('s8');
		$('.esc').addClass('s7');
		$(".mostrar,.esconder").hide();
	}

	if (apostilaOnline == 'off') {
		$('.verapostila').remove();
	}
	if (textobase == 'off') {
		$('.a1').remove();
		$('.setor1').remove();
	}
	if (videoaula == 'off') {
		$('.a2').remove();
		$('.setor2').remove();
	}
	if (infografico == 'off') {
		$('.a3').remove();
		$('.setor3').remove();
	}
	if (dicadoprofessor == 'off') {
		$('.a4').remove();
		$('.setor4').remove();
	}
	if (exercícios == 'off') {
		$('.a5').remove();
		$('.setor5').remove();
	}
	if (napratica == 'off') {
		$('.a6').remove();
		$('.setor6').remove();
	}
	if (saibamais == 'off') {
		$('.a7').remove();
		$('.setor7').remove();
	}
	//transcricao
	$(".mostrar").click(function () {
		$(".mostrar").hide();
		$(".esconder,.transcription").show();
		$('.esc').removeClass('s8');
		$('.esc').addClass('s7');
		return false;
	})
	$(".esconder").click(function () {
		$(".esconder,.transcription").hide();
		$(".mostrar").show();


		$('.esc').removeClass('s6');
		$('.esc').addClass('s8');
		return false;
	})
	$(".atv").click(function () {
		$('.telaBranca').fadeIn(500);
	})
	$(".sobrecaixa").click(function () {
		var img = $(this).data('goto');
		$('.telabranca2 img ').attr('src', img);


		$('.telabranca2').fadeIn(500);
	})

	//navegacao gambiarra

	$(".navegacao").click(function () {

		$('.sidenav,.sidenav-overlay').hide();
	})
	$(".sidenav-trigger").click(function () {

		$('.sidenav,.sidenav-overlay').show();
	})


	//apostila
	$(".lerapostila").click(function () {
		var livro = $(this).data('link');
		$('.telaBranca').fadeIn(500);
		$('.telaBranca').append('<iframe class="iframe" src="' + livro + '" width="100%" scrolling="auto" ></iframe>');
		$('html,body').scrollTop(0);
	})

	$(".conteudo_invisivel").on("click", ".veraba", function () {
		var valorDestino = $(this).data('destino');
		$('.abat').hide();
		$('.' + valorDestino).fadeIn();

	})
	$(".conteudo_invisivel").on("click", ".homes", function () {
		$(".conteudo_invisivel").hide();
		$(".conteudo_visivel").show();
	})


	$(".fecharBox").click(function () {
		$('.telaBranca').hide();
		$('.iframe').remove();
		$('html,body').scrollTop(0);
	})
	$(".fecharBox2").click(function () {
		$('.telaBranca2').hide();
		$('.iframe').remove();
		$('html,body').scrollTop(0);
	})

	$(".mtop").on("click", ".baixarApostila", function () {
		//$(".baixarApostila").click(function () {
		$('.conteudo_visivel').hide();
		$('.conteudo_invisivel').show();
		$.get("apostila_off.html", function (data) {
			$(".intos").html('carregando');
			$(".intos").html(data);
		});
	})

	if (window.matchMedia("(min-width: 992px)").matches) {
		/ the viewport is at least 992 pixels wide /
		$('.button-collapse').sideNav({
			closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
		});
	} else {
		/ the viewport is less than 992 pixels wide /
		$('.button-collapse').sideNav({
			closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
		});
	}

});
