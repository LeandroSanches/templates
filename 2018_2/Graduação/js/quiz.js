// JavaScript Document
$(document).ready(function() {


	//iniciando o quiz
	$('.q1,.avancar').delay(600).fadeIn(500);

	
	//ao clicar a alternativa muda de cor
	var respostas = [0];
	var gabaritos = [0];
	var respondidos = [];
	
	//
	//alert($('.q1 .resposta_a').data('justificativa'));
	
	
	
i = 1;

	
$(".resposta").click(function(){
	//pegar questao
	var questao =$(this).parent().data('questao');
	//pegar gabarito
	var gabarito =$(this).parent().find('.gabarito').text();
	// pegar cor da resposta
	
	$(".resposta").removeClass('respondido');
	$(this).addClass('respondido');
	
	// pegar valor da resposta dada
	var marcada =$(this).data('resposta');

	
	i = questao;
	
	//criar array para guardar as respostas
	respostas[questao] = marcada;
	gabaritos[questao] = gabarito;
	respondidos.push(questao);
	
	//teste
	
	if(marcada==gabarito){
		
		M.toast({html: '<i class="material-icons green-text text-darken-2">check</i> <span class="green-text text-darken-2"> Resposta Correta</span>',inDuration:100,displayLength:1000, classes:'rounded  grey lighten-5 black-text bordatoast'})
	}
	else{
		M.toast({html: '<i class="material-icons red-text text-darken-2">close</i> <span class="red-text text-darken-2"> Resposta Incorreta</span> ',inDuration:100,displayLength:1000, classes:'rounded grey lighten-5 black-text bordatoast'})
	}

	
});
	//sumir janela
	
	function sumir() {
    $('.erro').fadeOut(500);
}
	
	//botao avancar

$(".avancar").click(function(){	

	if(respostas[i]==null){
		$('.erro').fadeIn(500);
		$('.exibirErro').text('Por Favor escolha uma das alternativas');
		 setTimeout(sumir, 4000);

	}
	else{
	
	indice =i+1;
		indice2=i*20;
	i++;

		$('.evolucao2').animate({
				width: indice2 + '%'
		});
	$('.questao').hide();
	$('.q'+indice).show();
	if(indice>1){
	$('.voltar').fadeIn(500);
		$('.avancar').fadeIn(500);
		$('.concluir').hide();	
		
	}	
	
	if(indice==5){
		
		$('.avancar').hide();
		$('.concluir').fadeIn(500);
	}
}
});	
	//exibir paginacao
	
	
$(".voltar").click(function(){	

	indice =i-1;
	i--;
	$('.questao').hide();
	$('.q'+indice).fadeIn();
		if(indice<5){
			$('.concluir').hide();	
			$('.avancar').fadeIn(500);
			
		}	
		
	if(indice==1){
	
		$('.voltar').hide();	
	}
	
});	
	
		var t=0;
$(".concluir").click(function(){
	
	if(respostas[i]==null){

			$('.erro').fadeIn(500);
		$('.exibirErro').text('Por Favor escolha uma das alternativas');
		 setTimeout(sumir, 4000);
		
	}else{
		
		
	
	
		
	$('.questao,.concluir,.voltar,.avancar,.evolucao2  ').hide();
		$('.verREsultados').fadeIn(1000);
	
	if(respostas[1]==gabaritos[1]){t++;ms1='<i class="tiny  left material-icons green-text">check</i> Questão 01 - Você marcou a letra '+respostas[1].toUpperCase()+'  e acertou!'}else{ms1='<i class="tiny left  material-icons red-text">close</i> Questão 01 - Você marcou a letra '+respostas[1].toUpperCase()+'  e errou. O correto seria a letra '+gabaritos[1].toUpperCase()+'.'}
		
	if(respostas[2]==gabaritos[2]){t++;ms2='<i class="tiny  left material-icons green-text">check</i> Questão 02 - Você marcou a letra '+respostas[2].toUpperCase()+'  e acertou!'}else{ms2='<i class="tiny left  material-icons red-text">close</i> Questão 02 - Você marcou a letra '+respostas[2].toUpperCase()+'  e errou. O correto seria a letra '+gabaritos[2].toUpperCase()+'.'}
		
	if(respostas[3]==gabaritos[3]){t++;ms3='<i class="tiny left material-icons green-text">check</i> Questão 03 - Você marcou a letra '+respostas[3].toUpperCase()+'  e acertou!'}else{ms3='<i class="tiny left  material-icons red-text">close</i> Questão 03 - Você marcou a letra '+respostas[3].toUpperCase()+'  e errou. O correto seria a letra '+gabaritos[3].toUpperCase()+'.'}
		
	if(respostas[4]==gabaritos[4]){t++;ms4='<i class="tiny left  material-icons green-text">check</i> Questão 04 - Você marcou a letra '+respostas[1].toUpperCase()+'  e acertou!'}else{ms4='<i class="tiny left  material-icons red-text">close</i> Questão 04 - Você marcou a letra '+respostas[4].toUpperCase()+'  e errou. O correto seria a letra '+gabaritos[4].toUpperCase()+'.'}
		
	if(respostas[5]==gabaritos[5]){t++;ms5='<i class="tiny left  material-icons green-text">check</i> Questão 05 - Você marcou a letra '+respostas[5].toUpperCase()+'  e acertou!'}else{ms5='<i class="tiny left  material-icons red-text">close</i> Questão 05 - Você marcou a letra '+respostas[5].toUpperCase()+'  e errou. O correto seria a letra '+gabaritos[5].toUpperCase()+'.'}		
	
	 var media= (t*100)/5;
		

		
	
			$('.retorno').text('Você acertou '+t+' de 5 questões, com um aproveitamento de '+media+'%.');
			var perg1 = $(".q1 .pergunta").text();
			var perg2 = $(".q2 .pergunta").text();
			var perg3 = $(".q3 .pergunta").text();
			var perg4 = $(".q4 .pergunta").text();
			var perg5 = $(".q5 .pergunta").text();
		
	//respostas 1
			var resp1a = $(".q1 .resposta_a").html();
			var resp1b = $(".q1 .resposta_b").html();
			var resp1c = $(".q1 .resposta_c").html();
			var resp1d = $(".q1 .resposta_d").html();
			var resp1e = $(".q1 .resposta_e").html();	
		
				$('.aba1 .resp-a').html(resp1a);
				$('.aba1 .resp-b').html(resp1b);
				$('.aba1 .resp-c').html(resp1c);
				$('.aba1 .resp-d').html(resp1d);
				$('.aba1 .resp-e').html(resp1e);

		//respostas 2
			var resp2a = $(".q2 .resposta_a").html();
			var resp2b = $(".q2 .resposta_b").html();
			var resp2c = $(".q2 .resposta_c").html();
			var resp2d = $(".q2 .resposta_d").html();
			var resp2e = $(".q2 .resposta_e").html();	
				$('.aba2 .resp-a').html(resp2a);
				$('.aba2 .resp-b').html(resp2b);
				$('.aba2 .resp-c').html(resp2c);
				$('.aba2 .resp-d').html(resp2d);
				$('.aba2 .resp-e').html(resp2e);
		
		//respostas 3
			var resp3a = $(".q3 .resposta_a").html();
			var resp3b = $(".q3 .resposta_b").html();
			var resp3c = $(".q3 .resposta_c").html();
			var resp3d = $(".q3 .resposta_d").html();
			var resp3e = $(".q3 .resposta_e").html();
				$('.aba3 .resp-a').html(resp3a);
				$('.aba3 .resp-b').html(resp3b);
				$('.aba3 .resp-c').html(resp3c);
				$('.aba3 .resp-d').html(resp3d);
				$('.aba3 .resp-e').html(resp3e);

		//respostas 4
			var resp4a = $(".q4 .resposta_a").html();
			var resp4b = $(".q4 .resposta_b").html();
			var resp4c = $(".q4 .resposta_c").html();
			var resp4d = $(".q4 .resposta_d").html();
			var resp4e = $(".q4 .resposta_e").html();
				$('.aba4 .resp-a').html(resp4a);
				$('.aba4 .resp-b').html(resp4b);
				$('.aba4 .resp-c').html(resp4c);
				$('.aba4 .resp-d').html(resp4d);
				$('.aba4 .resp-e').html(resp4e);

		//respostas 5
			var resp5a = $(".q5 .resposta_a").html();
			var resp5b = $(".q5 .resposta_b").html();
			var resp5c = $(".q5 .resposta_c").html();
			var resp5d = $(".q5 .resposta_d").html();
			var resp5e = $(".q5 .resposta_e").html();	
				$('.aba5 .resp-a').html(resp5a);
				$('.aba5 .resp-b').html(resp5b);
				$('.aba5 .resp-c').html(resp5c);
				$('.aba5 .resp-d').html(resp5d);
				$('.aba5 .resp-e').html(resp5e);
		
	
		
		
		
		
		$('.perguntas1').html(perg1);
		$('.perguntas2').html(perg2);
		$('.perguntas3').html(perg3);
		$('.perguntas4').html(perg4);
		$('.perguntas5').html(perg5);
		
		
		$('.ms1').html(ms1);
		$('.ms2').html(ms2);
		$('.ms3').html(ms3);
		$('.ms4').html(ms4);
		$('.ms5').html(ms5);
		$('.grafico').html('<div id="piechart" style=" margin:auto; width: 390px; height: 390px;"></div>');
		
				 google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Questões', 'Aproveitamento'],
			['Acertos',      t],
          ['Erros',     5-t],
          

        ]);

        var options = {
          title: 'Gráfico de aproveitamento',
			 is3D: true
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }

	}	
});		
		$(".abas").click(function(){
		//avancarQuestoes(1);
		var destino =$(this).data('destino');
		$('.ab').fadeOut(500);
		$('.'+destino).delay(500).fadeIn(500);

	});

var x=0; //questoes certas
var y=0; //questoes marcadas
	$('.respostas_exibidas').hide();
	
	
	//PEGAR GABARITO

	var g1= $('.q1 .gabarito').text();
	var g2= $('.q2 .gabarito').text();
	var g3= $('.q3 .gabarito').text();

	var g4= $('.q4 .gabarito').text();
	var g5= $('.q5 .gabarito').text();

	$('.gav1 ').text(g1);
	$('.gav2 ').text(g2);
	$('.gav3 ').text(g3);
	$('.gav4 ').text(g4);
	$('.gav5 ').text(g5);

	$('.1resp'+g1).addClass('acertou');
	$('.2resp'+g2).addClass('acertou');
	$('.3resp'+g3).addClass('acertou');
	$('.4resp'+g4).addClass('acertou');
	$('.5resp'+g5).addClass('acertou');

	// colorir gabarito acertou

	// pegar respostas
	
	var q1la= $('.q1 .justificativa').data('justificativa');

	var q2la= $('.q2 .justificativa').data('justificativa');

	var q3la= $('.q3 .justificativa').data('justificativa');

	var q4la= $('.q4 .justificativa').data('justificativa');

	var q5la= $('.q5 .justificativa').data('justificativa');

	
	//mostrar respostas


	$('.respa1 ').html('<strong>JUSTIFICATIVA</strong>:<br/> '+q1la);

	$('.respa2 ').html('<strong>JUSTIFICATIVA</strong>:<br/> '+q2la);

	$('.respa3 ').html('<strong>JUSTIFICATIVA</strong>:<br/> '+q3la);

	$('.respa4 ').html('<strong>JUSTIFICATIVA</strong>:<br/> '+q4la);

	$('.respa5 ').html('<strong>JUSTIFICATIVA</strong>:<br/> '+q5la);

});
